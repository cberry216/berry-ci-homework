import unittest

from tip_calc import (
    get_tip_amount,
    get_tax_amount,
    get_subtotal
)


class TipCalcTestCase(unittest.TestCase):
    pass

    def test_get_tip_amount(self):
        total1 = 100
        tip_perc1 = 0.05
        tip_perc2 = "0.05"
        self.assertEqual(5, get_tip_amount(total1, tip_perc1))
        self.assertRaises(ValueError, get_tip_amount, total1, tip_perc2)

    def test_get_tax_amount(self):
        total1 = 100
        tax_amount1 = 0.075
        tax_amount2 = "0.075"
        self.assertEqual(7.5, get_tax_amount(total1, tax_amount1))
        self.assertRaises(ValueError, get_tax_amount, total1, tax_amount2)

    def test_get_subtotal(self):
        total1 = 100
        total2 = "100"
        self.assertEqual(107.5, get_subtotal(total1))
        self.assertRaises(ValueError, get_subtotal, total2)
