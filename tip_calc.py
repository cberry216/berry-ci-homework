def get_tip_amount(total, percentage):
    if type(total) == float or \
       type(total) == int and \
       type(percentage) == float:
        return total * percentage
    else:
        raise ValueError(f"total is of type {type(total)}."
                         f"percentage is of type {type(percentage)}.")


def get_tax_amount(total, tax):
    if type(total) == float or \
       type(total) == int and \
       type(tax) == float:
        return total * tax
    else:
        raise ValueError(f"total is of type {type(total)}. "
                         f"tax is of type {type(tax)}.")


def get_subtotal(total):
    if type(total) == float or \
       type(total) == int:
        return total + get_tax_amount(total, 0.075)
    else:
        raise ValueError(f"total is of type {type(total)}.")


def get_total(total, tip_percentage):
    if type(total) == float or \
       type(total) == int and \
       type(tip_percentage) == float:
        return get_subtotal(total) + get_tip_amount(total, tip_percentage)
    else:
        raise ValueError(f"total is of type {type(total)}."
                         f"tip_percentage is of type {type(tip_percentage)}.")
